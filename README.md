# Run environment

Everything running in docker's containers. I expect you have the docker installed.

## Database
I use exists container with port 5440 from host, because 5432 is occupied on my machine.
`docker run --rm -it -p 5440:5432 dockette/postgres:11`

- for international database i will use cluster locale C.UTF-8 instead of utf8.cs_CZ
- get container's ip address `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' optimistic_babbage`
  - for me is `172.17.0.2`
- save to `/etc/hosts` like
  - `172.17.0.2 cortex-db`
- connect to postgres server
  - `psql -U postgres -h cortex-db postgres`
- create user
  - `CREATE USER cortex WITH SUPERUSER PASSWORD 'cortex';`
  - for easy way i chose SUPERUSER privileges
- create database
  - `CREATE DATABASE cortex OWNER cortex;`

## Web server
- i have already installed, for this moment you must install yourself
- you can use php web server `php -S localhost:8080 -t web-app/www`

## Application
- base of application is nette/sandbox
- i add Translator for image what benefits has create form by factory 
- i add all vendor to repository it has few advantages
