DO $BODY$
  DECLARE
    v_user_id INTEGER;
    v_goods_id_1 INTEGER;
    v_goods_id_2 INTEGER;
    v_orders_id INTEGER;
    v_orders_goods_id_1 INTEGER;
    v_orders_goods_id_2 INTEGER;
  BEGIN
    -- DELETE FROM users WHERE email = 'foo@example.com';

    -- DELETE FROM goods WHERE code IN ('xx', 'yy');

    START TRANSACTION ;

    INSERT INTO users (email, password) VALUES ('foo@example.com', '') RETURNING id INTO v_user_id;

    INSERT INTO goods (code, price) VALUES ('xx', 1000) RETURNING id INTO v_goods_id_1;

    INSERT INTO goods (code, price) VALUES ('yy', 1250) RETURNING id INTO v_goods_id_2;

    INSERT INTO orders (users_id, mailing_addresses_id) VALUES (v_user_id, 2) RETURNING id INTO v_orders_id;

    -- INSERT
    INSERT INTO orders_goods (goods_id, piece, price, orders_id) VALUES (v_goods_id_1, 5, 1000, v_orders_id) RETURNING id INTO v_orders_goods_id_1;

    INSERT INTO orders_goods (goods_id, piece, price, orders_id) VALUES (v_goods_id_2, 4, 1250, v_orders_id) RETURNING id INTO v_orders_goods_id_2;

    IF (SELECT amount != 10000 FROM orders WHERE id = v_orders_id) THEN
      RAISE EXCEPTION 'Amount is not valid 10000.';
    END IF;

    UPDATE orders_goods SET price = 800 WHERE id = v_orders_goods_id_1;
    IF (SELECT amount != 9000 FROM orders WHERE id = v_orders_id) THEN
      RAISE EXCEPTION 'Amount is not valid 9000.';
    END IF;

    -- UPDATE
    UPDATE orders_goods SET piece = 3 WHERE id = v_orders_goods_id_1;
    IF (SELECT amount != 7400 FROM orders WHERE id = v_orders_id) THEN
      RAISE EXCEPTION 'Amount is not valid. 7400';
    END IF;

    -- DELETE
    DELETE FROM orders_goods WHERE id = v_orders_goods_id_1;
    IF (SELECT amount != 5000 FROM orders WHERE id = v_orders_id) THEN
      RAISE EXCEPTION 'Amount is not valid. 5000';
    END IF;

    DELETE FROM orders WHERE id = v_orders_id;

    ROLLBACK ;

  END
$BODY$;