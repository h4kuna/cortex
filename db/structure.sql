--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11 (Debian 10.11-1.pgdg90+1)
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-2.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: orders_report_most_used_card_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.orders_report_most_used_card_type AS (
	benefit_cards_id integer,
	date timestamp without time zone
);


--
-- Name: addresses_row_ai(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.addresses_row_ai() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  UPDATE users SET mailing_addresses_id = new.id WHERE id = new.users_id AND mailing_addresses_id IS NULL;

  RETURN new;
END
$$;


--
-- Name: addresses_row_bu(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.addresses_row_bu() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  v_is_assigned boolean;
  v_address_id integer;
BEGIN
  -- check if order has assigned addresses
  v_is_assigned = (EXISTS (SELECT 1 FROM orders WHERE users_id = new.users_id AND (mailing_addresses_id = new.id OR billing_addresses_id = new.id)));
  IF new.force_update = FALSE AND v_is_assigned THEN
    INSERT INTO addresses (users_id, streets_id, house_number) VALUES
      (new.users_id, new.streets_id, new.house_number) RETURNING id INTO v_address_id;

    -- check if user has assigned addresses
    v_is_assigned = EXISTS (SELECT 1 FROM users WHERE id = new.users_id AND (mailing_addresses_id = new.id OR billing_addresses_id = new.id));
    IF v_is_assigned THEN
      UPDATE users SET
        mailing_addresses_id = CASE WHEN mailing_addresses_id = new.id THEN v_address_id ELSE mailing_addresses_id END,
        billing_addresses_id = CASE WHEN billing_addresses_id = new.id THEN v_address_id ELSE billing_addresses_id END
      WHERE id = old.users_id;
    END IF;
    new = old;
    new.parent_id = v_address_id;
    RETURN new;
  END IF;
  new.force_update = FALSE;
  RETURN new;
END
$$;


--
-- Name: benefit_cards_row_biu(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.benefit_cards_row_biu() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  v_count smallint;
BEGIN
  IF TG_OP = 'UPDATE' THEN
    IF old.users_id IS NOT NULL AND new.users_id IS NOT NULL THEN
      RAISE EXCEPTION 'Change owner of card is not allowed, yet.';
    ELSEIF old.users_id IS NOT NULL AND new.users_id IS NULL THEN
      RAISE EXCEPTION 'Card must be deleted. Is not possible remove owner. Or any else use case?';
    ELSEIF old.users_id IS NULL AND new.users_id IS NOT NULL THEN
      new.assigned_datetime = current_timestamp;
    END IF;
    -- old.users_id IS NULL AND new.users_id IS NULL -- make nothing
  ELSEIF new.users_id IS NOT NULL THEN
    -- insert
    new.assigned_datetime = current_timestamp;
  END IF;

  RETURN new;
END
$$;


--
-- Name: orders_goods_row_aiud(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.orders_goods_row_aiud() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  r record;
BEGIN
  r = CASE TG_OP WHEN 'DELETE' THEN old ELSE new END;

  IF TG_OP = 'UPDATE' AND (old.piece != new.piece OR old.price != new.price) THEN
    UPDATE orders SET amount = (amount - (old.piece * old.price)) + (new.piece * new.price) WHERE id = old.orders_id;
  ELSEIF TG_OP != 'UPDATE' THEN -- INSERT OR DELETE
    UPDATE orders SET amount = amount + (r.piece * r.price * trigger_increment(TG_OP)) WHERE id = r.orders_id;
  END IF;

  RETURN r;
END
$$;


--
-- Name: orders_report_most_used_card_id(json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.orders_report_most_used_card_id(in_data json) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
  RETURN (
    SELECT benefit_cards_id
      FROM json_populate_recordset(NULL::ORDERS_REPORT_MOST_USED_CARD_TYPE, in_data)
      GROUP BY benefit_cards_id
      ORDER BY COUNT(benefit_cards_id) DESC, MAX(date) DESC
      LIMIT 1
  );
END
$$;


--
-- Name: random_between(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.random_between(low integer, high integer) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $$
BEGIN
  RETURN floor(random()* (high-low + 1) + low);
END;
$$;


--
-- Name: registrations_confirm(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.registrations_confirm(in_token character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
  v_users_id integer;
  v_inserted_datetime timestamptz;
BEGIN
  SELECT users_id, inserted_datetime INTO v_users_id, v_inserted_datetime
    FROM registrations
    WHERE token = in_token
    AND inserted_datetime > current_timestamp - interval '1 day';

  IF v_users_id IS NULL THEN
    RETURN FALSE;
  END IF;

  UPDATE users SET registration_date = v_inserted_datetime WHERE id = v_users_id;
  DELETE FROM registrations WHERE token = in_token;
  RETURN TRUE;
END
$$;


--
-- Name: registrations_row_bi(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.registrations_row_bi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  v_count smallint;
BEGIN
  v_count = (SELECT count(*) FROM registrations WHERE users_id = new.users_id);
  IF v_count > 5 THEN
    RAISE EXCEPTION 'Too many tries for restore password.';
  END IF;
  RETURN new;
END
$$;


--
-- Name: trigger_increment(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.trigger_increment(p_operation character varying) RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
BEGIN
  IF (p_operation = 'DELETE') THEN
    RETURN -1;
  ELSIF (p_operation = 'INSERT') THEN
    RETURN 1;
  ELSIF (p_operation = 'UPDATE') THEN
    RETURN 0;
  ELSE
    RAISE EXCEPTION 'Unknown trigger operation';
  END IF;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: addresses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.addresses (
    id integer NOT NULL,
    users_id integer NOT NULL,
    streets_id integer NOT NULL,
    house_number character varying NOT NULL,
    parent_id integer,
    force_update boolean DEFAULT false NOT NULL
);


--
-- Name: addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.addresses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.addresses_id_seq OWNED BY public.addresses.id;


--
-- Name: benefit_cards; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.benefit_cards (
    id integer NOT NULL,
    users_id integer,
    card_type_id smallint NOT NULL,
    code character varying NOT NULL,
    assigned_datetime timestamp with time zone,
    CONSTRAINT benefit_cards_card_type_id_check CHECK ((card_type_id = ANY (ARRAY[1, 2])))
);


--
-- Name: benefit_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.benefit_cards_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: benefit_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.benefit_cards_id_seq OWNED BY public.benefit_cards.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cities (
    id integer NOT NULL,
    zips_id integer NOT NULL,
    city character varying NOT NULL
);


--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cities_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cities_id_seq OWNED BY public.cities.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.countries (
    id integer NOT NULL,
    country character varying NOT NULL
);


--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.countries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;


--
-- Name: goods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.goods (
    id integer NOT NULL,
    code character varying NOT NULL,
    price integer NOT NULL,
    delete_datetime timestamp with time zone
);


--
-- Name: goods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.goods_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.goods_id_seq OWNED BY public.goods.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    users_id integer,
    billing_addresses_id integer,
    mailing_addresses_id integer NOT NULL,
    benefit_cards_id integer,
    inserted_date date DEFAULT CURRENT_DATE NOT NULL,
    inserted_time time without time zone DEFAULT CURRENT_TIME NOT NULL,
    amount integer DEFAULT 0 NOT NULL
);


--
-- Name: orders_goods; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orders_goods (
    id integer NOT NULL,
    goods_id integer NOT NULL,
    piece smallint NOT NULL,
    price integer NOT NULL,
    orders_id integer NOT NULL
);


--
-- Name: orders_goods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_goods_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_goods_id_seq OWNED BY public.orders_goods.id;


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: registrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.registrations (
    id integer NOT NULL,
    users_id integer NOT NULL,
    token character varying NOT NULL,
    inserted_datetime timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: registrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.registrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: registrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.registrations_id_seq OWNED BY public.registrations.id;


--
-- Name: streets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.streets (
    id integer NOT NULL,
    cities_id integer NOT NULL,
    street character varying NOT NULL
);


--
-- Name: streets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.streets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: streets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.streets_id_seq OWNED BY public.streets.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying,
    surname character varying,
    email character varying NOT NULL,
    phone character varying,
    registration_date timestamp with time zone,
    billing_addresses_id integer,
    mailing_addresses_id integer,
    password character varying NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: zips; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.zips (
    id integer NOT NULL,
    zip integer NOT NULL,
    countries_id integer NOT NULL
);


--
-- Name: view_addresses; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_addresses AS
 SELECT s.id AS streets_id,
    c.id AS cities_id,
    z.id AS zips_id,
    co.id AS countries_id,
    s.street,
    c.city,
    z.zip,
    co.country
   FROM (((public.countries co
     JOIN public.zips z ON ((co.id = z.countries_id)))
     JOIN public.cities c ON ((z.id = c.zips_id)))
     JOIN public.streets s ON ((c.id = s.cities_id)));


--
-- Name: view_users_purchase_turnover; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.view_users_purchase_turnover AS
 SELECT sum(o.amount) AS sum,
    public.orders_report_most_used_card_id(json_agg(json_build_object('date', ((o.inserted_date || ' '::text) || o.inserted_time), 'benefit_cards_id', o.benefit_cards_id))) AS benefit_cards_id,
    o.users_id
   FROM (public.orders o
     JOIN public.users u ON ((o.users_id = u.id)))
  WHERE ((o.inserted_date >= (CURRENT_DATE - 30)) AND (o.inserted_date <= CURRENT_DATE))
  GROUP BY o.users_id, u.id
  ORDER BY (sum(o.amount)) DESC
 LIMIT 10
  WITH NO DATA;


--
-- Name: zips_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.zips_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: zips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.zips_id_seq OWNED BY public.zips.id;


--
-- Name: addresses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses ALTER COLUMN id SET DEFAULT nextval('public.addresses_id_seq'::regclass);


--
-- Name: benefit_cards id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.benefit_cards ALTER COLUMN id SET DEFAULT nextval('public.benefit_cards_id_seq'::regclass);


--
-- Name: cities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cities ALTER COLUMN id SET DEFAULT nextval('public.cities_id_seq'::regclass);


--
-- Name: countries id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);


--
-- Name: goods id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.goods ALTER COLUMN id SET DEFAULT nextval('public.goods_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: orders_goods id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders_goods ALTER COLUMN id SET DEFAULT nextval('public.orders_goods_id_seq'::regclass);


--
-- Name: registrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.registrations ALTER COLUMN id SET DEFAULT nextval('public.registrations_id_seq'::regclass);


--
-- Name: streets id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.streets ALTER COLUMN id SET DEFAULT nextval('public.streets_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: zips id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zips ALTER COLUMN id SET DEFAULT nextval('public.zips_id_seq'::regclass);


--
-- Name: addresses addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_pkey PRIMARY KEY (id);


--
-- Name: benefit_cards benefit_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.benefit_cards
    ADD CONSTRAINT benefit_cards_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: countries countries_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: goods goods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.goods
    ADD CONSTRAINT goods_pkey PRIMARY KEY (id);


--
-- Name: orders_goods orders_goods_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders_goods
    ADD CONSTRAINT orders_goods_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: registrations registrations_id_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.registrations
    ADD CONSTRAINT registrations_id_pkey PRIMARY KEY (id);


--
-- Name: streets streets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.streets
    ADD CONSTRAINT streets_pkey PRIMARY KEY (id);


--
-- Name: users users_id_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_pkey PRIMARY KEY (id);


--
-- Name: zips zips_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zips
    ADD CONSTRAINT zips_pkey PRIMARY KEY (id);


--
-- Name: addresses_billing_addresses_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX addresses_billing_addresses_id_x ON public.orders USING btree (billing_addresses_id);


--
-- Name: addresses_mailing_addresses_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX addresses_mailing_addresses_id_x ON public.orders USING btree (mailing_addresses_id);


--
-- Name: addresses_parent_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX addresses_parent_id_x ON public.addresses USING btree (parent_id);


--
-- Name: addresses_streets_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX addresses_streets_id_x ON public.addresses USING btree (streets_id);


--
-- Name: addresses_users_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX addresses_users_id_x ON public.addresses USING btree (users_id);


--
-- Name: benefit_cards_code_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX benefit_cards_code_ukey ON public.benefit_cards USING btree (lower((code)::text));


--
-- Name: benefit_cards_users_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX benefit_cards_users_id_x ON public.benefit_cards USING btree (users_id);


--
-- Name: cities_city_zips_id_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX cities_city_zips_id_ukey ON public.cities USING btree (lower((city)::text), zips_id);


--
-- Name: cities_zips_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cities_zips_id_x ON public.cities USING btree (zips_id);


--
-- Name: countries_country_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX countries_country_ukey ON public.countries USING btree (lower((country)::text));


--
-- Name: goods_code_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX goods_code_ukey ON public.goods USING btree (lower((code)::text));


--
-- Name: orders_benefit_cards_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX orders_benefit_cards_id_x ON public.orders USING btree (benefit_cards_id);


--
-- Name: orders_goods_goods_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX orders_goods_goods_id_x ON public.orders_goods USING btree (goods_id);


--
-- Name: orders_inserted_date_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX orders_inserted_date_id_x ON public.orders USING btree (inserted_date);


--
-- Name: registrations_token_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX registrations_token_ukey ON public.registrations USING btree (token);


--
-- Name: registrations_users_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX registrations_users_id_x ON public.registrations USING btree (users_id);


--
-- Name: streets_cities_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX streets_cities_id_x ON public.streets USING btree (cities_id);


--
-- Name: streets_street_cities_id_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX streets_street_cities_id_ukey ON public.streets USING btree (lower((street)::text), cities_id);


--
-- Name: users_lower_email_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX users_lower_email_ukey ON public.users USING btree (lower((email)::text));


--
-- Name: view_users_purchase_turnover_benefit_cards_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX view_users_purchase_turnover_benefit_cards_id_x ON public.view_users_purchase_turnover USING btree (benefit_cards_id);


--
-- Name: view_users_purchase_turnover_sum_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX view_users_purchase_turnover_sum_x ON public.view_users_purchase_turnover USING btree (sum);


--
-- Name: view_users_purchase_turnover_users_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX view_users_purchase_turnover_users_id_x ON public.view_users_purchase_turnover USING btree (users_id);


--
-- Name: zips_countries_id_x; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX zips_countries_id_x ON public.zips USING btree (countries_id);


--
-- Name: zips_zip_countries_id_ukey; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX zips_zip_countries_id_ukey ON public.zips USING btree (zip, countries_id);


--
-- Name: addresses addresses_row_ai_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER addresses_row_ai_trigger AFTER INSERT ON public.addresses FOR EACH ROW EXECUTE PROCEDURE public.addresses_row_ai();


--
-- Name: addresses addresses_row_bu_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER addresses_row_bu_trigger BEFORE UPDATE ON public.addresses FOR EACH ROW EXECUTE PROCEDURE public.addresses_row_bu();


--
-- Name: benefit_cards benefit_cards_row_biu_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER benefit_cards_row_biu_trigger BEFORE INSERT OR UPDATE ON public.benefit_cards FOR EACH ROW EXECUTE PROCEDURE public.benefit_cards_row_biu();


--
-- Name: orders_goods orders_goods_row_aiud_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER orders_goods_row_aiud_trigger AFTER INSERT OR DELETE OR UPDATE ON public.orders_goods FOR EACH ROW EXECUTE PROCEDURE public.orders_goods_row_aiud();


--
-- Name: registrations registrations_row_bi_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER registrations_row_bi_trigger BEFORE INSERT ON public.registrations FOR EACH ROW EXECUTE PROCEDURE public.registrations_row_bi();


--
-- Name: addresses addresses_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: addresses addresses_streets_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_streets_id_fkey FOREIGN KEY (streets_id) REFERENCES public.streets(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: addresses addresses_users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.addresses
    ADD CONSTRAINT addresses_users_id_fkey FOREIGN KEY (users_id) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: benefit_cards benefit_cards_users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.benefit_cards
    ADD CONSTRAINT benefit_cards_users_id_fkey FOREIGN KEY (users_id) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: cities cities_zips_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_zips_id_fkey FOREIGN KEY (zips_id) REFERENCES public.zips(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: orders orders_benefit_cards_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_benefit_cards_id_fkey FOREIGN KEY (benefit_cards_id) REFERENCES public.benefit_cards(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: orders orders_billing_addresses_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_billing_addresses_id_fkey FOREIGN KEY (billing_addresses_id) REFERENCES public.addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: orders_goods orders_goods_goods_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders_goods
    ADD CONSTRAINT orders_goods_goods_id_fkey FOREIGN KEY (goods_id) REFERENCES public.goods(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: orders_goods orders_goods_orders_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders_goods
    ADD CONSTRAINT orders_goods_orders_id_fkey FOREIGN KEY (orders_id) REFERENCES public.orders(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: orders orders_mailing_addresses_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_mailing_addresses_id_fkey FOREIGN KEY (mailing_addresses_id) REFERENCES public.addresses(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: orders orders_users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_users_id_fkey FOREIGN KEY (users_id) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: registrations registrations_users_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.registrations
    ADD CONSTRAINT registrations_users_id_fkey FOREIGN KEY (users_id) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: streets streets_cities_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.streets
    ADD CONSTRAINT streets_cities_id_fkey FOREIGN KEY (cities_id) REFERENCES public.cities(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: users users_billing_addresses_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_billing_addresses_id_fkey FOREIGN KEY (billing_addresses_id) REFERENCES public.addresses(id) ON UPDATE RESTRICT ON DELETE SET NULL;


--
-- Name: users users_mailing_addresses_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_mailing_addresses_id_fkey FOREIGN KEY (mailing_addresses_id) REFERENCES public.addresses(id) ON UPDATE RESTRICT ON DELETE SET NULL;


--
-- Name: zips zips_countries_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.zips
    ADD CONSTRAINT zips_countries_id_fkey FOREIGN KEY (countries_id) REFERENCES public.countries(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

