<?php declare(strict_types=1);

namespace Web\App\Translate;

use h4kuna\Memoize\MemoryStorage;
use Web\App\Model\BenefitCard\Type;

final class BenefitCard
{
	use MemoryStorage;


	public function toText(int $cardId)
	{
		$cards = $this->memoize(__METHOD__, function (): array {
			return [
				Type::BASIC => 'základní',
				Type::TEMPORARY => 'Dočastná',
			];
		});
		return $cards[$cardId];
	}

}
