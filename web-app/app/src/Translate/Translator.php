<?php declare(strict_types=1);

namespace Web\App\Translate;

use Nette\Localization\ITranslator;

final class Translator implements ITranslator
{

	public function translate($message, ...$parameters): string
	{
		return (string) $message;
	}

}
