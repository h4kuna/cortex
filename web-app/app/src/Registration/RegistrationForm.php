<?php declare(strict_types=1);

namespace Web\App\Model\Registration;

use Nette\Application\UI;
use Nette\Forms;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\TextInput;
use Nette\Forms\Form;
use Web\App\Forms\FormFactory;
use Web\App\Model\Country;

final class RegistrationForm
{
	private const PASSWORD_MIN_LENGTH = 4;

	/** @var FormFactory */
	private $formFactory;

	/** @var Facade */
	private $facade;


	public function __construct(FormFactory $formFactory, Facade $facade)
	{
		$this->formFactory = $formFactory;
		$this->facade = $facade;
	}


	public function create(): UI\Form
	{
		$form = $this->formFactory->create();

		$form->addGroup('Základní informace');
		$this->userContainer($form->addContainer('user'));

		$form->addGroup('Doručovací Adresa');
		$this->addAddress($form->addContainer('mailing'));

		$form->addGroup('');
		$same = $form->addCheckbox('same_billing', 'Fakturační je stejná jako odesílací adresa');
		$same->setDefaultValue(TRUE)
			->addCondition($form::EQUAL, FALSE)
			->toggle('address-container');

		$group = $form->addGroup('Fakturační adresa');
		$group->setOption('id', 'address-container');
		$this->addAddress($form->addContainer('billing'), $same);

		$form->addGroup('');
		$form->addSelect('card', 'Zákaznická karta', $this->facade->benefitCards())
			->setPrompt('-- vyberte --')
			->setRequired('Vyberte typ kartičky.');
		$form->addSubmit('send', 'Registrovat');

		$form->onSuccess[] = function ($from, $values) {
			$token = $this->facade->save($values->user, $values->mailing, $values->same_billing ? NULL : $values->billing, $values->card);
			$this->facade->sendEmail($values->user, $token);
		};

		return $form;
	}


	private function userContainer(Forms\Container $form): void
	{
		$form->addText('name', 'Jméno')
			->setRequired('Jméno je povinné.');

		$form->addText('surname', 'Příjmení')
			->setRequired('Příjmení je povinné.');

		$form->addText('email', 'Email')
			->setRequired('Email je povinný.')
			->addRule(Form::EMAIL, 'Email není validní.')
			->addRule(function($input): bool {
				return $this->facade->checkEmail($input->getValue());
			}, 'Email je již obsazený.');

		$form->addText('phone', 'Telefon')
			->setNullable();

		$password = $this->addStrictPassword($form);
		$this->addPasswordCheck($form, $password);
	}


	public function addPasswordCheck(Forms\Container $form, TextInput $password): void
	{
		$input = $form->addPassword($password->getName() . '_check', 'Heslo ještě jednou');
		$input->setRequired(FALSE)
			->setOmitted()
			->addRule(UI\Form::EQUAL, 'Hesla se neshodují.', $password);
	}


	public function addStrictPassword(Forms\Container $form): TextInput
	{
		$input = $form->addPassword('password', 'Heslo');
		$input->setRequired('Heslo je povinné.')
			->addRule(UI\Form::MIN_LENGTH, 'Minimální delka hesla je %s znaků.', self::PASSWORD_MIN_LENGTH)
			->addRule(UI\Form::PATTERN, 'Heslo musí obsahovat číslice.', '.*[0-9].*');
		return $input;
	}


	private function addAddress(Forms\Container $form, Checkbox $same = NULL): void
	{
		self::setRequire($form->addText('street', 'Ulice'), $same, 'Ulice je povinná.');

		self::setRequire($form->addText('house_number', 'Číslo popisné'), $same, 'Číslo popisné je povinné.');

		self::setRequire($form->addText('city', 'Město'), $same, 'Město je povinné.');

		$zip = $form->addText('zip', 'PSČ');
		self::setRequire($zip, $same, 'PSČ je povinné.');
		$zip->addCondition(Form::FILLED, TRUE)
			->addRule(Form::PATTERN, 'Zadejte pouze čísla a mezery.', '[ 0-9]+');
		$form->addText('country', 'Země')
			->getControlPrototype()->placeholder = Country\Repository::DEFAULT_COUNTRY;
	}


	private static function setRequire(TextInput $input, ?Checkbox $same, string $text): void
	{
		($same === NULL ? $input : $input->addConditionOn($same, Form::EQUAL, FALSE))->setRequired($text);
	}

}
