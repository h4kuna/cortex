<?php declare(strict_types=1);

namespace Web\App\Model\Registration;

use h4kuna\MailManager\MailManager;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;
use Web\App;
use Web\App\Database\Transaction;
use Web\App\Model\Registration;
use Web\App\Model\User;

final class Facade
{

	/** @var User\Repository */
	private $userRepository;

	/** @var App\Model\BenefitCard\Repository */
	private $cardRepository;

	/** @var App\User */
	private $user;

	/** @var Registration\Repository */
	private $registrationRepository;

	/** @var Transaction */
	private $transaction;

	/** @var Passwords */
	private $password;

	/** @var MailManager */
	private $mailManager;


	public function __construct(
		User\Repository $userRepository,
		App\Model\BenefitCard\Repository $cardRepository,
		App\User $user,
		Repository $registrationRepository,
		Transaction $transaction,
		Passwords $password,
		MailManager $mailManager
	)
	{
		$this->userRepository = $userRepository;
		$this->cardRepository = $cardRepository;
		$this->user = $user;
		$this->registrationRepository = $registrationRepository;
		$this->transaction = $transaction;
		$this->password = $password;
		$this->mailManager = $mailManager;
	}


	public function save(ArrayHash $user, ArrayHash $mailingAddress, ?ArrayHash $billingAddress, int $cardId): string
	{
		return $this->transaction->execute(function () use ($user, $mailingAddress, $billingAddress, $cardId) {
			$token = Random::generate(40);
			$user->password = $this->password->hash($user->password);

			$userId = $this->userRepository->insertReturning((array) $user, ['id'])->id;

			$this->cardRepository->save($userId, $cardId);

			$this->user->addAddress($userId, $mailingAddress->street, $mailingAddress->house_number, $mailingAddress->city, self::zip($mailingAddress->zip), $mailingAddress->country);

			if ($billingAddress !== NULL) {
				$this->user->addAddress($userId, $billingAddress->street, $billingAddress->house_number, $billingAddress->city, self::zip($billingAddress->zip), $billingAddress->country);
			}

			$this->registrationRepository->save($userId, $token);
			return $token;
		});
	}


	public function sendEmail(ArrayHash $user, string $token)
	{
		$message = $this->mailManager->createMessage(sprintf('vytvořit odkaz s tokenem: %s', $token));
		$message->addTo($user->email, trim($user->name . ' ' . $user->surname));
		$this->mailManager->send($message);
	}


	private static function zip(string $zip): int
	{
		return (int) str_replace(' ', '', $zip);
	}


	public function checkEmail(string $email): bool
	{
		return $this->userRepository->findByEmail($email) === NULL;
	}


	public function benefitCards(): array
	{
		return [
			App\Model\BenefitCard\Type::BASIC => 'základní',
			App\Model\BenefitCard\Type::TEMPORARY => 'dočasná',
		];
	}

}
