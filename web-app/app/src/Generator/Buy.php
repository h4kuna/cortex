<?php declare(strict_types=1);

namespace Web\App\Generator;

use Forrest79\PhPgSql\Db\Row;
use Web\App\Generator;
use Web\App\Model\Address;
use Web\App\Model\BenefitCard;
use Web\App\Model\Goods;
use Web\App\Model\Order;
use Web\App\Model\User;

final class Buy
{
	/** @var User\Repository */
	private $user;

	/** @var BenefitCard\Repository */
	private $benefitsCardRepository;

	/** @var Address\Repository */
	private $addressRepository;

	/** @var Goods\Repository */
	private $goodsRepository;

	/** @var Generator\Address */
	private $addressGenerator;

	/** @var Generator\Goods */
	private $goodsGenerator;

	/** @var Cards */
	private $cardGenerator;

	/** @var Order\Repository */
	private $orderRepository;

	/** @var Order\Goods\Repository */
	private $orderGoodsRepository;


	public function __construct(
		User\Repository $user,
		BenefitCard\Repository $benefitsCardRepository,
		Address\Repository $addressRepository,
		Goods\Repository $goodsRepository,
		\Web\App\Generator\Address $addressGenerator,
		\Web\App\Generator\Goods $goodsGenerator,
		Cards $cardGenerator,
		Order\Repository $orderRepository,
		Order\Goods\Repository $orderGoodsRepository
	)
	{
		$this->user = $user;
		$this->benefitsCardRepository = $benefitsCardRepository;
		$this->addressRepository = $addressRepository;
		$this->goodsRepository = $goodsRepository;
		$this->addressGenerator = $addressGenerator;
		$this->goodsGenerator = $goodsGenerator;
		$this->cardGenerator = $cardGenerator;
		$this->orderRepository = $orderRepository;
		$this->orderGoodsRepository = $orderGoodsRepository;
	}


	public function selectUser(): Row
	{
		// subquery
		$sql = $this->user->confirmed()
			->select(['id' => 'random_between(min(id), max(id))']);

		return $this->user->confirmed()
			->select(['id', 'billing_addresses_id', 'mailing_addresses_id'])
			->where('id >= (?)', $sql)
			->orderBy('id')
			->limit(1)
			->fetch();
	}


	public function chooseAddress(int $userId, ?int $addressesId, ?int $ignoreAddressId = NULL): int
	{
		$addresses = $this->loadAddresses($userId, $ignoreAddressId);
		if (rand(0, 10) === 0 || $addresses === []) {
			$addressesId = $this->addressGenerator->generate($userId);
		} else {
			shuffle($addresses);
			$addressesId = reset($addresses);
		}
		return $addressesId;
	}


	public function basket(int $orderId)
	{
		$items = rand(1, 20);
		$goods = [];
		for ($i = 0; $i < $items; ++$i) {
			$item = $this->goods();
			$goods[] = [
				'goods_id' => $item->id,
				'piece' => rand(1, 20),
				'price' => $item->price,
				'orders_id' => $orderId,
			];
		}
		$this->orderGoodsRepository->table()->rows($goods)->execute();
	}


	private function goods(): Row
	{
		$sql = $this->goodsRepository->visible()
			->select(['id' => 'random_between(min(id), max(id))']);

		$goods = $this->goodsRepository->visible()
			->select(['id', 'price'])
			->where('id >= (?)', $sql)
			->limit(1)
			->fetch();

		if ($goods === NULL) {
			$this->goodsGenerator->generate(100);
			return $this->goods();
		}
		return $goods;
	}


	public function benefitCard(int $userId, bool $wantCard): ?int
	{
		$card = $this->loadCard($userId);
		if ($card === NULL && $wantCard) {
			$card = $this->freeBenefitCard();
			if ($card === NULL) {
				$this->cardGenerator->generate();
				$card = $this->freeBenefitCard();
			}
			$this->benefitsCardRepository->update($card->id, [
				'users_id' => $userId,
			]);
		}

		return $card->id ?? NULL;
	}


	private function loadCard(int $userId): ?Row
	{
		return $this->benefitsCardRepository->findCard($userId)
			->select(['id'])
			->orderBy('card_type_id')
			->limit(1)
			->fetch();
	}


	public function order(Row $user): int
	{
		return $this->orderRepository->insertReturning([
			'users_id' => $user->id,
			'billing_addresses_id' => $user->billing_addresses_id,
			'mailing_addresses_id' => $user->mailing_addresses_id,
			'benefit_cards_id' => $user->benefit_cards_id,
		], ['id'])->id;
	}


	private function freeBenefitCard(): ?Row
	{
		return $this->benefitsCardRepository->findCard(NULL)->select(['id'])->limit(1)->fetch();
	}


	public function saveAddressAsFavorite(int $userId, ?int $mailingAddressesId, ?int $billingAddressesId): void
	{
		$this->user->update($userId, [
			'billing_addresses_id' => $billingAddressesId,
			'mailing_addresses_id' => $mailingAddressesId,
		]);
	}


	private function loadAddresses(int $userId, ?int $ignoreAddressId): array
	{
		$sql = $this->addressRepository->findByUser($userId)
			->select(['id']);
		if ($ignoreAddressId !== NULL) {
			$sql->where('id != ?', $ignoreAddressId);
		}

		return $sql->fetchPairs(NULL, 'id');
	}

}
