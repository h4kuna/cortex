<?php declare(strict_types=1);

namespace Web\App\Generator;

use Forrest79\PhPgSql\Db\Row;
use Nette\Utils\Random;
use Web\App\Database\Transaction;
use Web\App\Model;

final class User
{
	/** @var Model\User\Repository */
	private $userRepository;

	/** @var Model\Registration\Repository */
	private $registrationRepository;

	/** @var Transaction */
	private $transaction;


	public function __construct(
		Model\User\Repository $userRepository,
		Model\Registration\Repository $registrationRepository,
		Transaction $transaction
	)
	{
		$this->userRepository = $userRepository;
		$this->registrationRepository = $registrationRepository;
		$this->transaction = $transaction;
	}


	public function generate(): Row
	{
		return $this->transaction->execute(function (): Row {
			$name = Random::generate();
			$user = $this->userRepository->insertReturning([
				'email' => $name . '@example.com',
				'name' => $name,
				'surname' => $name,
				'password' => Random::generate(20),
				'phone' => '1' . Random::generate(8, '0-9'),
			], ['id', 'name']);
			$registration = $this->registrationRepository->insertReturning([
				'users_id' => $user->id,
				'token' => Random::generate(30),
			], ['token', 'inserted_datetime']);

			$user->token = $registration->token;
			$user->inserted_datetime = $registration->inserted_datetime;
			return $user;
		});
	}

}
