<?php declare(strict_types=1);

namespace Web\App\Generator;

use Web\App;
use Web\App\Model\Country;

final class Address
{
	/** @var App\User */
	private $user;


	public function __construct(App\User $user)
	{
		$this->user = $user;
	}


	public function generate(int $userId): int
	{
		$city = 'City' . rand(0, 20);
		$street = 'Street' . rand(0, 50);
		$houseNumber = self::houseNumber();
		$zip = rand(11111, 99999);
		return $this->user->addAddress($userId, $street, $houseNumber, $city, $zip, Country\Repository::DEFAULT_COUNTRY);
	}


	private static function houseNumber(): string
	{
		$prefix = rand(1, 100);
		$sufix = rand(0, 10);
		return $prefix . ($sufix === 0 ? '' : ('/' . $sufix));
	}

}
