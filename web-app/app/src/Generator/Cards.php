<?php declare(strict_types=1);

namespace Web\App\Generator;

use Web\App\Model\BenefitCard;

final class Cards
{
	/** @var BenefitCard\Repository */
	private $benefitCard;


	public function __construct(BenefitCard\Repository $benefitCard)
	{
		$this->benefitCard = $benefitCard;
	}


	public function generate(int $typeId = BenefitCard\Type::BASIC, int $count = 1): int
	{
		$count = min(max(1, $count), 10000);
		$typeId = BenefitCard\Type::get($typeId)->value();
		$rows = [];
		for ($i = 0; $i < $count; ++$i) {
			$rows[] = [
				'card_type_id' => $typeId,
				'code' => $this->benefitCard::generateCode(),
			];
		}

		$this->benefitCard->table()->rows($rows)->execute();
		return $count;
	}

}
