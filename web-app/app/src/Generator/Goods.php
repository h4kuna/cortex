<?php declare(strict_types=1);

namespace Web\App\Generator;

use Nette\Utils\Random;
use Web\App\Model;

final class Goods
{
	/** @var Model\Goods\Repository */
	private $goods;


	public function __construct(Model\Goods\Repository $goods)
	{
		$this->goods = $goods;
	}


	public function generate(int $count = 1): int
	{
		$count = min(max(1, $count), 10000);
		$rows = [];
		for ($i = 0; $i < $count; ++$i) {
			$rows[] = [
				'code' => str_replace('o', '0', Random::generate()),
				'price' => rand(100, 10000000), // 1 - 100.000
			];
		}

		$this->goods->table()->rows($rows)->execute();
		return $count;
	}

}
