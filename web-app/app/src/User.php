<?php declare(strict_types=1);

namespace Web\App;

use Web\App\Database\Transaction;

final class User
{
	/** @var Address */
	private $address;

	/** @var Model\Address\Repository */
	private $addressRepository;

	/** @var Transaction */
	private $transaction;


	public function __construct(
		Address $address,
		Model\Address\Repository $addressRepository,
		Transaction $transaction
	)
	{
		$this->address = $address;
		$this->addressRepository = $addressRepository;
		$this->transaction = $transaction;
	}


	public function addAddress(
		int $userId,
		string $street,
		string $houseNumber,
		string $city,
		int $zip,
		string $country
	): int
	{
		return $this->transaction->execute(function () use ($userId, $street, $houseNumber, $city, $zip, $country): int {
			$streetId = $this->address->save($street, $city, $zip, $country);
			return $this->addressRepository->insertReturning([
				'users_id' => $userId,
				'house_number' => $houseNumber,
				'streets_id' => $streetId,
			], ['id'])->id;
		});
	}

}
