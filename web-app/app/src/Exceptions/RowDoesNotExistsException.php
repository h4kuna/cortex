<?php declare(strict_types=1);

namespace Web\App\Exceptions;

final class RowDoesNotExistsException extends \InvalidArgumentException
{

}
