<?php declare(strict_types=1);

namespace Web\App\Database;

use Forrest79\PhPgSql;
use h4kuna\Memoize\MemoryStorage;

class Connection extends PhPgSql\Fluent\Connection
{
	use MemoryStorage;


	public function transaction(): PhPgSql\Db\Transaction
	{
		return $this->memoize(__METHOD__, function (): Transaction {
			return new Transaction($this);
		});
	}

}
