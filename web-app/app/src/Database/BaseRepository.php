<?php declare(strict_types=1);

namespace Web\App\Database;

use Forrest79\PhPgSql;
use Forrest79\PhPgSql\Fluent;

abstract class BaseRepository
{
	/** @var Fluent\Connection */
	private $connection;


	public function __construct(Fluent\Connection $connection)
	{
		$this->connection = $connection;
	}


	public function table(?string $alias = NULL): Fluent\Query
	{
		return $this->createQuery()->table(static::getTableName(), $alias);
	}


	/**
	 * @param mixed[] $columns
	 * @param string|NULL $alias
	 * @return Fluent\Query
	 */
	public function select(array $columns, ?string $alias = NULL): Fluent\Query
	{
		return $this->table($alias)->select($columns);
	}


	/**
	 * @param int $id
	 * @param string[] $columns
	 * @return PhPgSql\Db\Row|NULL
	 */
	public function get(int $id, array $columns): ?PhPgSql\Db\Row
	{
		return $this->select($columns)
			->where(static::getPrimaryKey(), $id)
			->limit(1)
			->fetch();
	}


	/**
	 * @param mixed[] $data
	 * @return PhPgSql\Db\Result
	 */
	public function insert(array $data): PhPgSql\Db\Result
	{
		return $this->table()->values($data)->execute();
	}


	/**
	 * @param mixed[] $data
	 * @param string[] $returning
	 * @return PhPgSql\Db\Row
	 */
	public function insertReturning(array $data, array $returning): PhPgSql\Db\Row
	{
		$row = $this->table()->values($data)->returning($returning)->fetch();
		if ($row === NULL) {
			throw new DatabaseException('No row was inserted.');
		}
		return $row;
	}


	/**
	 * @param int $id
	 * @param mixed[] $data
	 * @return PhPgSql\Db\Result
	 */
	public function update(int $id, array $data): PhPgSql\Db\Result
	{
		return $this->table()->set($data)->where(static::getPrimaryKey(), $id)->execute();
	}


	/**
	 * @param int $id
	 * @param mixed[] $data
	 * @param string[] $returning
	 * @return PhPgSql\Db\Row
	 */
	public function updateReturning(int $id, array $data, array $returning): PhPgSql\Db\Row
	{
		$row = $this->table()->set($data)->where(static::getPrimaryKey(), $id)->returning($returning)->fetch();
		if ($row === NULL) {
			throw new DatabaseException('No row was updated.');
		}
		return $row;
	}


	public function delete(int $id): PhPgSql\Db\Result
	{
		return $this->table()->delete()->where(static::getPrimaryKey(), $id)->execute();
	}


	/**
	 * @param int $id
	 * @param string[] $returning
	 * @return PhPgSql\Db\Row
	 */
	public function deleteReturning(int $id, array $returning): PhPgSql\Db\Row
	{
		$row = $this->table()->delete()->where(static::getPrimaryKey(), $id)->returning($returning)->fetch();
		if ($row === NULL) {
			throw new DatabaseException('No row was deleted.');
		}
		return $row;
	}


	protected function connection(): Connection
	{
		return $this->connection;
	}


	protected function createQuery(): Fluent\QueryExecute
	{
		return $this->connection->createQuery();
	}


	abstract public static function getTableName(): string;


	public static function getPrimaryKey(): string
	{
		return 'id';
	}


	public static function getDefaultJoinColumn(): string
	{
		throw new DatabaseException(sprintf('%s repository has no default join column', static::class));
	}


	/**
	 * @param class-string $repositoryClass
	 * @param string|NULL $joinColumn
	 * @return string[]
	 */
	public static function meta(string $repositoryClass, ?string $joinColumn): array
	{
		/** @var self $repositoryClass */
		return [
			$repositoryClass::getTableName(),
			$joinColumn ?? $repositoryClass::getDefaultJoinColumn(),
		];
	}


	protected static function withAlias(string $column, ?string $alias): string
	{
		return Sql::withAlias($column, $alias);
	}

}
