<?php declare(strict_types=1);

namespace Web\App\Database;

use Forrest79\PhPgSql\Db;

abstract class Sql
{

	public static function withAlias(string $column, ?string $alias): string
	{
		return ($alias === '') || ($alias === NULL)
			? $column
			: ($alias . '.' . $column);
	}


	public static function literal(string $value): Db\Sql\Literal
	{
		return new Db\Sql\Literal($value);
	}


	public static function now(): Db\Sql\Literal
	{
		return self::literal('CURRENT_TIMESTAMP');
	}


	/**
	 * @param string $value
	 * @param mixed[] ...$params
	 */
	public static function parameters(string $value, ...$params): Db\Sql\Expression
	{
		return new Db\Sql\Expression($value, $params);
	}

}
