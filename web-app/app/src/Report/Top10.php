<?php declare(strict_types=1);

namespace Web\App\Report;

use Forrest79\PhPgSql\Fluent\QueryExecute;
use Web\App\Model\Address;
use Web\App\Model\BenefitCard;
use Web\App\Model\User;
use Web\App\Model\View\Addresses;
use Web\App\Model\View\UsersPurchaseTurnover;

final class Top10
{
	/** @var UsersPurchaseTurnover */
	private $userPurchaseTurnover;


	public function __construct(UsersPurchaseTurnover $userPurchaseTurnover)
	{
		$this->userPurchaseTurnover = $userPurchaseTurnover;
	}


	public function report(): QueryExecute
	{
		return $this->userPurchaseTurnover->table('upt')
			->select([
				'sum',
				'fullname' => User\Repository::fullname('u'),
				'u.id',
				'address' => Addresses::fullAddress('a.house_number', 'va'),
				'bc.code',
				'bc.card_type_id',
			])
			->join(User\Repository::getTableName(), 'u', 'upt.users_id = u.id')
			->leftJoin(BenefitCard\Repository::getTableName(), 'bc', 'upt.benefit_cards_id = bc.id')
			->leftJoin(Address\Repository::getTableName(), 'a', 'u.mailing_addresses_id = a.id')
			->leftJoin(Addresses::getTableName(), 'va', 'a.streets_id = va.streets_id')
			->orderBy('sum DESC')
			->limit(10);
	}

}
