<?php declare(strict_types=1);

namespace Web\App\ReportDetail;

use Forrest79\PhPgSql\Db\Row;
use Web\App\Database\Sql;
use Web\App\Exceptions\RowDoesNotExistsException;
use Web\App\Model\Address;
use Web\App\Model\BenefitCard;
use Web\App\Model\User;
use Web\App\Model\View\Addresses;
use Web\App\Model\View\UsersPurchaseTurnover;

final class Facade
{
	/** @var UsersPurchaseTurnover */
	private $usersPurchaseTurnover;

	/** @var Address\Repository */
	private $addressRepository;

	/** @var User\Repository */
	private $userRepository;

	/** @var BenefitCard\Repository */
	private $benefitCardRepository;


	public function __construct(
		UsersPurchaseTurnover $usersPurchaseTurnover,
		Address\Repository $addressRepository,
		User\Repository $userRepository,
		BenefitCard\Repository $benefitCardRepository
	)
	{
		$this->usersPurchaseTurnover = $usersPurchaseTurnover;
		$this->addressRepository = $addressRepository;
		$this->userRepository = $userRepository;
		$this->benefitCardRepository = $benefitCardRepository;
	}


	/**
	 * @throws RowDoesNotExistsException
	 */
	public function customer(int $userId): ?Customer
	{
		$purchaseTurnover = $this->loadTurnover($userId);
		$user = $this->loadUser($userId);
		$addresses = $this->loadAddresses($userId, $user->mailing_addresses_id, $user->billing_addresses_id);
		$cards = $this->loadCards($userId, $purchaseTurnover->benefit_cards_id);

		return new Customer($purchaseTurnover->sum, $user, $cards, $addresses);
	}


	/**
	 * @throws RowDoesNotExistsException
	 */
	private function loadTurnover(int $userId): Row
	{
		$turnover = $this->usersPurchaseTurnover
			->findByUserId($userId)
			->select(['sum', 'benefit_cards_id'])
			->fetch();
		if ($turnover === NULL) {
			throw new RowDoesNotExistsException(sprintf('Turn over for user: %s', $userId));
		}
		return $turnover;
	}


	/**
	 * @throws RowDoesNotExistsException
	 */
	private function loadUser(int $userId): ?Row
	{
		$user = $this->userRepository->select([
			'fullname' => $this->userRepository::fullname(),
			'email',
			'phone',
			'registration_date',
			'billing_addresses_id',
			'mailing_addresses_id',
		])
			->where('id', $userId)
			->fetch();

		if ($user === NULL) {
			throw new RowDoesNotExistsException(sprintf('User does not exists: %s', $userId));
		}

		return $user;
	}


	private function loadAddresses(int $userId, ?int $mailingAddressesId, ?int $billingAddressesId): array
	{
		return $this->addressRepository->table('a')
			->select([
				'address' => Addresses::fullAddress('a.house_number', 'va'),
				'is_mailing' => Sql::parameters('a.id = ?', $mailingAddressesId),
				'is_billing' => Sql::parameters('a.id = ?', $billingAddressesId),
			])
			->join(Addresses::getTableName(), 'va', 'a.id = va.streets_id AND parent_id IS NULL')
			->where('users_id', $userId)
			->orderBy(Sql::parameters('array_position(ARRAY[?, ?]::int[], a.id)', $mailingAddressesId, $billingAddressesId))
			->fetchAll();
	}


	private function loadCards(int $userId, ?int $benefitCardsId): array
	{
		return $this->benefitCardRepository->select([
			'code',
			'card_type_id',
			'assigned_datetime',
			'is_primary' => Sql::parameters('id = ?', $benefitCardsId),
		])
			->where('users_id', $userId)
			->orderBy(Sql::parameters('array_position(ARRAY[?]::int[], id)', $benefitCardsId))
			->fetchAll();
	}

}
