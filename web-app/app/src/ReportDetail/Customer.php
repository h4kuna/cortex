<?php declare(strict_types=1);

namespace Web\App\ReportDetail;

use Forrest79\PhPgSql\Db\Row;

final class Customer
{
	/** @var int */
	public $turnover;

	/** @var Row */
	public $user;

	/** @var Row[] */
	public $cards = [];

	/** @var Row[] */
	public $addresses = [];


	public function __construct(int $turnover, Row $user, array $cards, array $addresses)
	{
		$this->turnover = $turnover;
		$this->user = $user;
		$this->cards = $cards;
		$this->addresses = $addresses;
	}

}
