<?php declare(strict_types=1);

namespace Web\App;

use Nette\Utils\Strings;
use Web\App\Database\Transaction;
use Web\App\Model;

final class Address
{
	/** @var Model\Street\Repository */
	private $street;

	/** @var Model\Zip\Repository */
	private $zip;

	/** @var Model\City\Repository */
	private $city;

	/** @var Model\Country\Repository */
	private $country;

	/** @var Transaction */
	private $transaction;


	public function __construct(
		Model\Street\Repository $street,
		Model\Zip\Repository $zip,
		Model\City\Repository $city,
		Model\Country\Repository $country,
		Transaction $transaction
	)
	{
		$this->street = $street;
		$this->zip = $zip;
		$this->city = $city;
		$this->country = $country;
		$this->transaction = $transaction;
	}


	public function save(string $street, string $city, int $zip, string $country): int
	{
		if ($country === '' || Strings::lower($country) === 'česko') {
			$country = Model\Country\Repository::DEFAULT_COUNTRY;
		}

		return $this->transaction->execute(function () use ($street, $city, $zip, $country): int {
			$countryId = $this->country->save($country);
			$zipId = $this->zip->save($zip, $countryId);
			$cityId = $this->city->save($city, $zipId);
			$streetId = $this->street->save($street, $cityId);
			return $streetId;
		});
	}

}
