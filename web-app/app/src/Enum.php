<?php declare(strict_types=1);

namespace Web\App;

abstract class Enum
{
	/** @var mixed */
	private $value;

	/** @var array<static[]> */
	private static $instances = [];


	/**
	 * @param mixed $value
	 */
	final private function __construct($value)
	{
		self::checkValue($value);
		$this->value = $value;
	}


	/**
	 * @param mixed $value
	 * @return static
	 */
	public static function get($value)
	{
		if ($value !== NULL && !is_scalar($value)) {
			throw new Exceptions\UnexpectedValueException(sprintf('Value must be scalar, given %s', gettype($value)));
		}
		if (!isset(self::$instances[static::class][$value])) {
			self::$instances[static::class][$value] = new static($value);
		}
		return self::$instances[static::class][$value];
	}


	/**
	 * @return mixed
	 */
	public function value()
	{
		return $this->value;
	}


	/**
	 * @return mixed[]
	 */
	abstract public static function values(): array;


	/**
	 * @param mixed $value
	 */
	private static function checkValue($value): void
	{
		if (!self::has($value)) {
			throw new Exceptions\UnexpectedValueException(
				sprintf(
					'Value "%s" (%s) don\'t belong to enum of [%s]',
					is_scalar($value) ? $value : gettype($value),
					is_scalar($value) || is_array($value) ? gettype($value) : get_class($value),
					implode(', ', static::printableValues()),
				),
			);
		}
	}


	/**
	 * @return string[]
	 */
	final public static function printableValues(): array
	{
		return array_map(static function ($value): string {
			if ($value === NULL) {
				return 'NULL';
			} else if (is_bool($value)) {
				return $value ? 'TRUE' : 'FALSE';
			}
			return (string) $value;
		}, static::values());
	}


	/**
	 * @param mixed $value
	 * @return bool
	 */
	public static function has($value): bool
	{
		return in_array($value, static::values(), TRUE);
	}


	/**
	 * @param static|mixed $value
	 * @return bool
	 */
	public function equals($value): bool
	{
		if ($value instanceof self) {
			return $value === $this;
		}
		return $this->value === $value;
	}


	public function __toString(): string
	{
		return serialize($this);
	}

}
