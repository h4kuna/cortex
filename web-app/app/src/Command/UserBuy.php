<?php declare(strict_types=1);

namespace Web\App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Web\App\Generator;

final class UserBuy extends Command
{
	protected static $defaultName = 'user:buy';

	/** @var Generator\Buy */
	private $buy;


	public function __construct(Generator\Buy $buy)
	{
		parent::__construct();
		$this->buy = $buy;
	}


	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$user = $this->buy->selectUser();
		$user->mailing_addresses_id = $this->buy->chooseAddress($user->id, $user->mailing_addresses_id);

		if (rand(0, 1) === 1) {
			$user->billing_addresses_id = $this->buy->chooseAddress($user->id, NULL);
		}
		$user->benefit_cards_id = $this->buy->benefitCard($user->id, rand(0, 1) === 1);
		$orderId = $this->buy->order($user);
		$this->buy->basket($orderId);

		if (rand(0, 1) === 1) {
			$this->buy->saveAddressAsFavorite($user->id, $user->mailing_addresses_id, $user->billing_addresses_id);
		}

		return 0;
	}

}
