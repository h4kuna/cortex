<?php declare(strict_types=1);

namespace Web\App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Web\App\Model;

final class ReportCustomers extends Command
{
	protected static $defaultName = 'report:customers';

	/** @var Model\View\UsersPurchaseTurnover */
	private $usersTurnover;


	public function __construct(Model\View\UsersPurchaseTurnover $usersTurnover)
	{
		parent::__construct();
		$this->usersTurnover = $usersTurnover;
	}


	protected function configure()
	{
		$this->setDescription('Refresh view: view_users_purchase_turnover');
	}


	protected function execute(InputInterface $input, OutputInterface $output): ?int
	{
		$this->usersTurnover->refresh();
		return 0;
	}

}
