<?php declare(strict_types=1);

namespace Web\App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Web\App\Generator;
use Web\App\Model\BenefitCard\Type;

final class GenerateCards extends Command
{

	protected static $defaultName = 'generate:cards';

	/** @var Generator\Cards */
	private $cards;


	public function __construct(Generator\Cards $cards)
	{
		parent::__construct();
		$this->cards = $cards;
	}


	protected function configure()
	{
		$this->addArgument('count', InputArgument::OPTIONAL, 'How many cards to create?', 1)
			->addOption('temporary', 't', InputOption::VALUE_NONE, 'Create temporary cards.');
	}


	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$type = $input->getOption('temporary') ? Type::TEMPORARY : Type::BASIC;
		$count = $this->cards->generate($type, (int) $input->getArgument('count'));

		$output->writeln(sprintf('Byla vygenerování %s karet.', $count));// neskloňuju

		return 0;
	}

}
