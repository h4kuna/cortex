<?php declare(strict_types=1);

namespace Web\App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Web\App\Generator;
use Web\App\Model;

final class GenerateUser extends Command
{
	protected static $defaultName = 'generate:user';

	/** @var Model\Registration\Repository */
	private $registrationRepository;

	/** @var Generator\User */
	private $user;

	/** @var Generator\Address */
	private $address;


	public function __construct(
		Model\Registration\Repository $registrationRepository,
		Generator\User $user,
		Generator\Address $address
	)
	{
		parent::__construct();
		$this->registrationRepository = $registrationRepository;
		$this->user = $user;
		$this->address = $address;
	}


	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$userId = $this->insertUser($output);
		if (rand(0, 1) === 1) {
			$this->address->generate($userId);
		}
		return 0;
	}


	private function insertUser(OutputInterface $output): int
	{
		$user = $this->user->generate();
		$confirm = $this->registrationRepository->isTokenValid($user->token);
		$output->writeln(sprintf('Dne %s se registroval uživatel %s s id %s.', self::formatDate($user->inserted_datetime), $user->name, $user->id));

		if ($confirm) {
			$output->writeln('Potvrzení registrace se zdařilo.');
		}

		return $user->id;
	}


	private static function formatDate(\DateTimeInterface $date): string
	{
		return $date->format('j.n. H:i');
	}

}
