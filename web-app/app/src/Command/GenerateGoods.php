<?php declare(strict_types=1);

namespace Web\App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Web\App\Generator;

final class GenerateGoods extends Command
{

	protected static $defaultName = 'generate:goods';

	/** @var Generator\Goods */
	private $goods;


	public function __construct(Generator\Goods $cards)
	{
		parent::__construct();
		$this->goods = $cards;
	}


	protected function configure()
	{
		$this->addArgument('count', InputArgument::OPTIONAL, 'How many cards to create?', 1);
	}


	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$count = $this->goods->generate((int) $input->getArgument('count'));

		$output->writeln(sprintf('Bylo vygenerováno %s ks zboží.', $count));// neskloňuju

		return 0;
	}

}
