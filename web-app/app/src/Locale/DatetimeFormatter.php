<?php declare(strict_types=1);

namespace Web\App\Model\Locale;

final class DatetimeFormatter
{

	public function datetime(\DateTimeInterface $dateTime): string
	{
		return $dateTime->format('j. n. Y H:i');
	}

}
