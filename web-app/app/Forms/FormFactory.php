<?php declare(strict_types=1);

namespace Web\App\Forms;

use Nette\Application\UI;
use Nette\Localization\ITranslator;

final class FormFactory
{

	/** @var ITranslator */
	private $translator;


	public function __construct(ITranslator $translator)
	{
		$this->translator = $translator;
	}


	public function create(bool $protection = TRUE): UI\Form
	{
		$form = new UI\Form;
		if ($protection) {
			$form->addProtection($this->translator->translate('Token vypršel opakujte odeslání formuláře.'));
		}
		self::makeBootstrap4($form);
		$form->setTranslator($this->translator);
		return $form;
	}


	private static function makeBootstrap4(UI\Form $form): void
	{
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class="form-group row"';
		$renderer->wrappers['pair']['.error'] = 'has-danger';
		$renderer->wrappers['control']['container'] = 'div class=col-sm-9';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-3 col-form-label"';
		$renderer->wrappers['control']['description'] = 'span class=form-text';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=form-control-feedback';
		$renderer->wrappers['control']['.error'] = 'is-invalid';

		foreach ($form->getControls() as $control) {
			$type = $control->getOption('type');
			if ($type === 'button') {
				$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-secondary');
				$usedPrimary = TRUE;
			} else if (in_array($type, ['text', 'textarea', 'select'], TRUE)) {
				$control->getControlPrototype()->addClass('form-control');
			} else if ($type === 'file') {
				$control->getControlPrototype()->addClass('form-control-file');
			} else if (in_array($type, ['checkbox', 'radio'], TRUE)) {
				if ($control instanceof \Nette\Forms\Controls\Checkbox) {
					$control->getLabelPrototype()->addClass('form-check-label');
				} else {
					$control->getItemLabelPrototype()->addClass('form-check-label');
				}
				$control->getControlPrototype()->addClass('form-check-input');
				$control->getSeparatorPrototype()->setName('div')->addClass('form-check');
			}
		}
	}

}
