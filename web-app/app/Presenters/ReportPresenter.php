<?php declare(strict_types=1);

namespace Web\App\Presenters;

use Web\App\Report\Top10;

final class ReportPresenter extends BasePresenter
{
	/** @var Top10 */
	private $top10;


	public function __construct(Top10 $top10)
	{
		parent::__construct();
		$this->top10 = $top10;
	}


	public function renderDefault()
	{
		$this->getTemplate()->report = $this->top10->report();
	}

}
