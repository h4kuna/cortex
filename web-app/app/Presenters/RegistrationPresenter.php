<?php declare(strict_types=1);

namespace Web\App\Presenters;

use Web\App\Model\Registration\RegistrationForm;

final class RegistrationPresenter extends BasePresenter
{
	/** @var RegistrationForm */
	private $registrationForm;


	public function __construct(RegistrationForm $registrationForm)
	{
		parent::__construct();
		$this->registrationForm = $registrationForm;
	}


	protected function createComponentRegistrationForm()
	{
		$form = $this->registrationForm->create();
		$form->onSuccess[] = function () {
			$this->flashMessage('Registrace by úspěšná a byl "odeslán" potvrzovací email, byl uložen do temp adresáře.');
			$this->redirect(':Homepage:');
		};
		return $form;
	}

}
