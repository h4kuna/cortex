<?php declare(strict_types=1);

namespace Web\App\Presenters;

use Web\App\Exceptions\RowDoesNotExistsException;
use Web\App\ReportDetail;

final class ReportDetailPresenter extends BasePresenter
{
	/**
	 * @param int
	 * @persistent
	 */
	public $id = 0;

	/** @var ReportDetail\Facade */
	private $reportDetailFacade;


	public function __construct(ReportDetail\Facade $reportDetailFacade)
	{
		parent::__construct();
		$this->reportDetailFacade = $reportDetailFacade;
	}


	public function renderDefault(): void
	{
		$template = $this->getTemplate();
		try {
			$template->customer = $customer = $this->reportDetailFacade->customer($this->id);
		} catch (RowDoesNotExistsException $e) {
			$this->error($e->getMessage());
		}
	}

}
