<?php declare(strict_types=1);

namespace Web\App\Model\Street;

use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'streets';
	}


	public function save(string $street, int $cityId): int
	{
		$streetId = $this->select(['id'])
			->where('lower(street) = lower(?)', $street)
			->where('cities_id', $cityId)
			->fetchSingle();

		if ($streetId === NULL) {
			$data = $this->insertReturning([
				'street' => $street,
				'cities_id' => $cityId,
			], ['id']);
			$streetId = $data->id;
		}
		return $streetId;
	}

}
