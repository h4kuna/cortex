<?php declare(strict_types=1);

namespace Web\App\Model\User;

use Forrest79\PhPgSql\Db\Row;
use Forrest79\PhPgSql\Fluent;
use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'users';
	}


	public function confirmed(): Fluent\Query
	{
		return $this->table()->where('registration_date IS NOT NULL');
	}


	public function login(string $email, string $password): ?Row
	{
		return $this->select(['id', 'registration_date'])
			->where('email', $email)
			->where('password', $password)
			->fetch();
	}


	public function findByEmail(string $email): ?Row
	{
		return $this->select(['id'])
			->where('lower(email) = lower(?)', $email)
			->limit(1)
			->fetch();
	}


	public static function fullname(string $alias = NULL): string
	{
		return sprintf("trim(%s || '  ' || %s)", self::withAlias('name', $alias), self::withAlias('surname', $alias));
	}

}
