<?php declare(strict_types=1);

namespace Web\App\Model\Address;

use Forrest79\PhPgSql\Fluent\Query;
use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'addresses';
	}


	public function findByUser(int $userId): Query
	{
		return $this->active()->where('users_id', $userId);
	}


	public function active(): Query
	{
		return $this->table()->where('parent_id IS NULL');
	}

}
