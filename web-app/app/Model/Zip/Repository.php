<?php declare(strict_types=1);

namespace Web\App\Model\Zip;

use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'zips';
	}


	public function save(int $zip, int $countryId): int
	{
		$zipId = $this->select(['id'])
			->where('zip', $zip)
			->where('countries_id', $countryId)
			->fetchSingle();

		if ($zipId === NULL) {
			$data = $this->insertReturning([
				'zip' => $zip,
				'countries_id' => $countryId,
			], ['id']);
			$zipId = $data->id;
		}
		return $zipId;
	}

}
