<?php declare(strict_types=1);

namespace Web\App\Model\View;

use Forrest79\PhPgSql\Fluent\QueryExecute;
use Web\App\Database\BaseRepository;

final class UsersPurchaseTurnover extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'view_users_purchase_turnover';
	}


	public function findByUserId(int $userId): QueryExecute
	{
		return $this->table()->where('users_id', $userId);
	}


	public function refresh(): void
	{
		$this->connection()->query(sprintf('REFRESH MATERIALIZED VIEW CONCURRENTLY %s', self::getTableName()));
	}

}
