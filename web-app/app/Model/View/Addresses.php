<?php declare(strict_types=1);

namespace Web\App\Model\View;

use Web\App\Database\BaseRepository;

final class Addresses extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'view_addresses';
	}


	public static function fullAddress(string $houseNumber, string $alias = NULL): string
	{
		return sprintf("%s || ' ' || %s || ', ' || %s || ', ' || %s || ', ' || %s",
			self::withAlias('street', $alias),
			$houseNumber,
			self::withAlias('city', $alias),
			self::withAlias('zip', $alias),
			self::withAlias('country', $alias)
		);
	}

}
