<?php declare(strict_types=1);

namespace Web\App\Model\Country;

use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{
	public const DEFAULT_COUNTRY = 'Česká republika';

	public static function getTableName(): string
	{
		return 'countries';
	}


	public function save(string $country): int
	{
		$countryId = $this->select(['id'])
			->where('lower(country) = lower(?)', $country)
			->fetchSingle();

		if ($countryId === NULL) {
			$data = $this->insertReturning([
				'country' => $country
			], ['id']);
			$countryId = $data->id;
		}
		return $countryId;
	}

}
