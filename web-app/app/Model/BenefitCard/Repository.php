<?php declare(strict_types=1);

namespace Web\App\Model\BenefitCard;

use Forrest79\PhPgSql\Fluent\Query;
use Nette\Utils\Random;
use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'benefit_cards';
	}


	public function findCard(?int $userId): Query
	{
		return $this->table()->where('users_id', $userId);
	}


	public function save(int $userId, int $typeId): void
	{
		$type = Type::get($typeId);
		$this->insert([
			'card_type_id' => $type->value(),
			'users_id' => $userId,
			'code' => self::generateCode(),
		]);
	}


	public static function generateCode(): string
	{
		return str_replace('o', '0', Random::generate());
	}

}
