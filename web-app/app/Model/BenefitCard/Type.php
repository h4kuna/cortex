<?php declare(strict_types=1);

namespace Web\App\Model\BenefitCard;

final class Type extends \Web\App\Enum
{

	public const BASIC = 1;
	public const TEMPORARY = 2;


	public static function values(): array
	{
		return [
			self::BASIC,
			self::TEMPORARY,
		];
	}

}
