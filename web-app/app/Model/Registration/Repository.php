<?php declare(strict_types=1);

namespace Web\App\Model\Registration;

use Web\App\Database\BaseRepository;
use Web\App\Database\Sql;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'registrations';
	}


	public function isTokenValid(string $token): bool
	{
		return $this->select(['valid' => Sql::parameters('registrations_confirm(?)', $token)])->fetchSingle();
	}


	public function save(int $userId, string $token): void
	{
		$this->insert([
			'users_id' => $userId,
			'token' => $token
		]);
	}

}
