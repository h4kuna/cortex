<?php declare(strict_types=1);

namespace Web\App\Model\Goods;

use Forrest79\PhPgSql\Fluent\Query;
use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'goods';
	}


	public function visible(): Query
	{
		return $this->table()->where('delete_datetime IS NULL');
	}

}
