<?php declare(strict_types=1);

namespace Web\App\Model\Order\Goods;

use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'orders_goods';
	}

}
