<?php declare(strict_types=1);

namespace Web\App\Model\City;

use Web\App\Database\BaseRepository;

final class Repository extends BaseRepository
{

	public static function getTableName(): string
	{
		return 'cities';
	}


	public function save(string $city, int $zipId): int
	{
		$cityId = $this->select(['id'])
			->where('lower(city) = lower(?)', $city)
			->where('zips_id', $zipId)
			->fetchSingle();

		if ($cityId === NULL) {
			$data = $this->insertReturning([
				'city' => $city,
				'zips_id' => $zipId,
			], ['id']);
			$cityId = $data->id;
		}
		return $cityId;
	}

}
